// Setup
function phoneticLookup(val) {
  let result = '';

  // Only change code below this line
  // switch (val) {
  //   case 'alpha':
  //     result = 'Adams';
  //     break;
  //   case 'bravo':
  //     result = 'Boston';
  //     break;
  //   case 'charlie':
  //     result = 'Chicago';
  //     break;
  //   case 'delta':
  //     result = 'Denver';
  //     break;
  //   case 'echo':
  //     result = 'Easy';
  //     break;
  //   case 'foxtrot':
  //     result = 'Frank';
  // }
  var lookup = {
    "alpha": "Adams",
    "bravo": "Boston",
    "charlie": "Chicago",
    "delta": "Denver",
    "echo": "Easy",
    "foxtrot": "Frank"
};
  // Only change code above this line
  result = lookup[val]
  return result;
}

console.log(phoneticLookup('charlie')); // Chicago

