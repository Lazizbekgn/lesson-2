// Factory Function
function factoryCard(picture, title, descr, price, count) {
    return {
        picture,
        title,
        descr,
        price,
        count
    }
}

const a = factoryCard("picture.jpg", "Burger", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem sequi maxime eligendi sunt natus commodi?",15000, "+")
const b = factoryCard("picture1.jpg", "hot god", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem sequi maxime eligendi sunt natus commodi?",25000, "+")
const c = factoryCard("picture2.jpg", "sandwich", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem sequi maxime eligendi sunt natus commodi?",35000, "+")

console.log(a);
console.log(b);
console.log(c);

// Constructor Function
function ConstCard(picture, title, descr, price, count) {
    this.picture = picture
    this.title = title
    this.descr = descr
    this.price = price
    this.count = count
}

const burger = new ConstCard("picture.jpg", "Burger", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem sequi maxime eligendi sunt natus commodi?",15000, "+")
const hotDog = new ConstCard("picture.jpg", "Burger", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem sequi maxime eligendi sunt natus commodi?",25000, "+")
const sandwich = new ConstCard("picture.jpg", "Burger", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem sequi maxime eligendi sunt natus commodi?",35000, "+")
console.log(burger);
console.log(hotDog);
console.log(sandwich);